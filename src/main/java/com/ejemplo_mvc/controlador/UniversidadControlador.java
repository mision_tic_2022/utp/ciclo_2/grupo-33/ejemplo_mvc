package com.ejemplo_mvc.controlador;

import java.util.ArrayList;

import com.ejemplo_mvc.modelo.Universidad;

public class UniversidadControlador {

    // ATRIBUTOS
    private ArrayList<Universidad> universidades;

    // CONSTRUCTOR
    public UniversidadControlador() {
        universidades = new ArrayList<Universidad>();
    }

    // CONSULTORES
    public int getCantidadUniversidades() {
        return universidades.size();
    }

    // ACCIONES
    public void crearUniversidad(String nit, String nombre, String direccion, String email) {
        // Crear Universidad
        Universidad objUni = new Universidad(nit, nombre, direccion, email);
        // Almacenar universidad
        universidades.add(objUni);
    }

    public String obtenerUniversidadXindex(int index) {
        // Obtener universidad por index
        Universidad objUni = universidades.get(index);
        // Retornar universidad
        return objUni.toString();
    }

}
